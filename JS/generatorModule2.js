/**
 * Created by leletir on 20/12/2016.
 */
var currentAction={name:"",date_start:"",date_end:""};

function generator(id, graphManager, ticker, oldest_date, newest_date, socket)
{
    let divOld = document.createElement('div');
    divOld.textContent = oldest_date;
    divOld.className = 'oldDate';

    let divNew = document.createElement('div');
    divNew.textContent = newest_date;
    divNew.className = 'newDate';

    let name = document.createElement('div');
    name.textContent = ticker;
    name.className = 'buttonModule2';
    name.onclick = (function(){
        if(currentAction.name != "")
        {
            currentAction.name.style.color = 'white';
            currentAction.date_start.style.color = 'white';
            currentAction.date_end.style.color = 'white';
        }
        currentAction.name = name;
        currentAction.date_start = this.dn;
        currentAction.date_end = this.do;
        this.gm.drawGraph(1,this.id);
        name.style.color = "red";
        this.do.style.color = "red";
        this.dn.style.color = "red";
        actionObjet.cleanAllExceptMarket();
        var generatorModule3InfoObjet = new GeneratorModule3Info(socket, this.id, this.ticker);
        actionObjet.setTicker(this.ticker); //on récupère le ticker et on le met dans notre objet action
    }).bind({gm: graphManager,id: id,ticker: ticker, do: divOld,dn: divNew});

    let img = document.createElement('img');
    img.src = 'img/equity.png';
    img.className ="image_sizing";

    var divGlobal = document.createElement('div');
    divGlobal.className = "instrument";
    divGlobal.appendChild(img);
    divGlobal.appendChild(name);
    divGlobal.appendChild(divOld);
    divGlobal.appendChild(divNew);

    return divGlobal;
}