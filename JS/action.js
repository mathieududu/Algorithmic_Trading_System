/**
 * Created by leletir on 22/12/2016.
 */
class action {
    constructor(){
        this.ticker_name = undefined;
        this.ticker = undefined;
        this.ticker_id = undefined;
        this.market_name = undefined;
        this.low = undefined;
        this.high = undefined;
        this.date_start = undefined;
        this.date_end = undefined;
        this.money_account = undefined;
        this.close = undefined;
        this.currency = undefined;
        this.timeZone = undefined;
        this.open = undefined;
    }

    setTickerName(ticker_name)
    {
        this.ticker_name = ticker_name;
        //console.log(this.ticker_name);
    }

    setTicker(ticker)
    {
        this.ticker= ticker;
        //console.log(this.ticker);
    }

    setTickerId(id)
    {
        this.ticker_id = id;
    }

    setClose(close)
    {
        this.close = close;
    }

    setLow_High(close)//on cherche le min et max pour la période selectionnée
    {
        let IndexOfDateStart = close.findIndex((function (elt) {
            return elt[0] >= (new Date(this.date_start)).getTime();
        }).bind(this));
        this.date_start = moment(close[IndexOfDateStart][0]).format("YYYY-MM-DD");

        let IndexOfDateEnd = close.findIndex((function (elt) {
            return elt[0] >= (new Date(this.date_end)).getTime();
        }).bind(this));
        this.date_end = moment(close[IndexOfDateEnd][0]).format("YYYY-MM-DD");


        let close_slice = close.slice(IndexOfDateStart,IndexOfDateEnd);
        this.low = this.high = close_slice[0][1];
        close_slice.forEach((function (row) {
            if(row[1] > this.high)
            {
                this.high = row[1];
            }
            else if(row[1] < this.low)
            {
                this.low = row[1]
            }
        }).bind(this));
    }

    setDates(date_start, date_end)
    {
        this.date_end = moment(date_end).format("YYYY-MM-DD");
        this.date_start = moment(date_start).format("YYYY-MM-DD");
        this.setLow_High(this.close);

    }

    setMoneyAccount(money_account)
    {
        this.money_account = money_account;
    }

    setMarketName(market_name)
    {
        this.market_name = market_name;
    }

    setCurrency(currency)
    {
        this.currency = currency;
    }

    setTimeZone(timeZone,id)
    {
        this.timeZone = timeZone;
        this.isMarketOpen(timeZone,id)
    }

    isMarketOpen(timeZone, id)
    {
        let mm = new Date();
        let date =  moment(mm).format("YYYY-MM-DD");
        let time = ((new Date((new Date()).getTime() + (parseInt(timeZone.slice(0,3)))*1000 * 3600)).getHours())*3600*1000 + ((new Date((new Date()).getTime() + (parseInt(actionObjet.timeZone.slice(0,3)))*1000 * 3600)).getMinutes())*60*1000 + ((new Date((new Date()).getTime() + (parseInt(actionObjet.timeZone.slice(0,3)))*1000 * 3600)).getSeconds())*1000;
        if(moment(date).day() == 6 || moment(date).day() == 6) //NYSE
        {
            this.open = false;
        }
        else if (!(id == 6 || id == 5 || id == 4))
        {
            if(time > (9.5*3600*1000) && time < (16*3600*1000)) //Ouverture NYSE 9h Fermeture 16h
            {
                this.open = true;
            }
            else
            {
                this.open = false;
            }
        }
        else
        {
            if(time > (9*3600*1000) && time < (17.5*3600*1000)) // Ouverture Europe: 9h Fermeture: 17h30
            {
                this.open = true;
            }
            else
            {
                this.open = false;
            }
        }
    }

    cleanAll()
    {
        this.ticker_name = undefined;
        this.ticker = undefined;
        this.market_name = undefined;
        this.low = undefined;
        this.high = undefined;
        this.date_start = undefined;
        this.date_end = undefined;
        this.money_account = undefined;
    }

    cleanAllExceptMarket()
    {
        this.ticker_name = undefined;
        this.ticker = undefined;
        this.low = undefined;
        this.high = undefined;
        this.date_start = undefined;
        this.date_end = undefined;
        this.money_account = undefined;
    }

}