/**
 * Created by leletir on 20/12/2016.
 */

class GeneratorModule3Info {
    constructor(socket,id,ticker) {
        this.socket = socket;
        this.p_instr = document.querySelector('#p_instr');
        this.ticker_name = document.querySelector('#ticker_name');
        this.p_sector = document.querySelector('#p_sector');
        this.buttonTrade = document.querySelector('#button_trade');
        this.recherche = document.querySelector('#recherche');
        this.recherche.style.visibility  = 'visible';
        this.buttonTrade.style.visibility = 'visible';
        this.ticket = document.querySelector('#ticket');
        this.div_cache = document.querySelector('#cache_trade');
        this.buttonTrade.onclick = (function() {
            this.ticket.style.visibility = 'visible';
            this.div_cache.style.visibility = 'visible';
            let resultQuerySelectorAllTspan = document.querySelectorAll('tspan');
            actionObjet.setDates(resultQuerySelectorAllTspan[0].textContent,resultQuerySelectorAllTspan[1].textContent); // On récupère la date start en date end et on les mets dans l'objet action
            generatorTicketObjet.generateTicket();
        }).bind(this);
        this.p_instr.textContent = ticker;
        this.id = id;
        actionObjet.setTickerId(this.id);
        this.ticker = ticker;
        this.recuperationInfoIntoDb(this.id);
        this.socket.on('retour_search_info', (function(data) {
            this.ticker_name.textContent = data.name;
            actionObjet.setTickerName(data.name); // On récupère le ticker name et on le met dans l'objet action
            this.p_sector.textContent = data.sector;
        }).bind(this));
    }

    recuperationInfoIntoDb(id)
    {
        this.socket.emit('search_info', id);
    }
}