/**
 * Created by leletir on 22/12/2016.
 */
class generatorTicket {
    constructor(socket){
        this.button_ticket_cancel = document.querySelector('#button_ticket_cancel');
        this.button_ticket_trade = document.querySelector('#button_ticket_trade');
        this.name_ticker_ticket = document.querySelector('#name_ticker_ticket');
        this.ticker_ticket = document.querySelector('#ticker_ticket');
        this.account = document.querySelector('#account');
        this.currency = document.querySelector('#currency');
        this.low_high = document.querySelector('#low_high');
        this.market_time = document.querySelector('#market_time');
        this.market_name = document.querySelector('#market_name');
        this.start = document.querySelector('#start');
        this.end = document.querySelector('#end');
        this.stop_loss = document.querySelector('#stop_loss');
        this.share = document.querySelector('#shares_ticket');
        this.market_open = document.querySelector('#market_open');
        this.div_cache = document.querySelector('#cache_trade');

        //Tableau gérant les strats
        this.tab_strat = undefined;

        //Liste déoulante
        this.strategy = document.querySelector('#strat_ticket');
        this.strategy.onchange = (function (e) {
            this.selectOptions(e);
        }.bind(this));
        this.option = document.querySelector('#options_ticket');

        //Gestion socket
        this.socket = socket;
        this.socket.emit('requireStratJSON');
        this.socket.on('sendStratJSON',(function (stratJSON) {
            this.stratJSONReader(stratJSON);
        }).bind(this));
        this.ticket = document.querySelector('#ticket');

        //Bouton cancel
        this.button_ticket_cancel.onclick = (function () {
            this.ticket.style.visibility = 'hidden';
            this.div_cache.style.visibility = 'hidden';
            this.market_open.style.visibility = 'hidden';
        }).bind(this);

        //Button trade
        this.button_ticket_trade.onclick = (function () {
            this.trade();
            this.ticket.style.visibility = 'hidden';
            this.div_cache.style.visibility = 'hidden';
            this.market_open.style.visibility = 'hidden';
        }).bind(this);

    }

    generateTicket()
    {
        this.name_ticker_ticket.textContent = actionObjet.ticker_name;
        this.ticker_ticket.textContent = actionObjet.ticker;
        this.account.textContent = actionObjet.money_account;
        this.low_high.textContent = actionObjet.low+"/"+actionObjet.high;
        this.market_name.textContent = actionObjet.market_name;
        this.start.textContent = actionObjet.date_start;
        this.end.textContent = actionObjet.date_end;
        this.currency.textContent = actionObjet.currency;
        let time = (new Date((new Date()).getTime() + (parseInt(actionObjet.timeZone.slice(0,3)))*1000 * 3600)).getHours() +" h "+ (new Date((new Date()).getTime() + (parseInt(actionObjet.timeZone.slice(0,3)))*1000 * 3600)).getMinutes() +" m "+ (new Date((new Date()).getTime() + (parseInt(actionObjet.timeZone.slice(0,3)))*1000 * 3600)).getSeconds()+" s";
        this.market_time.textContent = time;
        if(actionObjet.open == false)
        {
            this.market_open.style.visibility = 'visible';//faire apparaître un truc rouge market close
        }
        //Stategy + options
        // Voyant market Close
        // Stop Loss
        // Bouton trade
    }

    stratJSONReader(strats)
    {
        this.tab_strat  =strats;
        //let strats_json = JSON.parse(strats);
        this.tab_strat.forEach((function (elt) {
            let strat_read = document.createElement('option');
            strat_read.textContent = elt.name;
            strat_read.value = elt.name;
            this.strategy.appendChild(strat_read);
        }).bind(this));
        let option = this.tab_strat[0].options;
        this.option.innerHTML = ""; // on clean le tableau
        for(var i in option)
        {
            let option_read = document.createElement('option');
            option_read.textContent = option[i];
            option_read.value = option[i];
            this.option.appendChild(option_read);
        }
    }

    selectOptions(event)
    {
        let target = event.target;
        let index = target.selectedIndex;
        let option = this.tab_strat[index].options;
        this.option.innerHTML = ""; // on clean le tableau
        for(var i in option)
        {
            let option_read = document.createElement('option');
            option_read.textContent = option[i];
            option_read.value = option[i];
            this.option.appendChild(option_read);
        }
    }

    trade()
    {
        var stop_loss_value = this.stop_loss.value;
        var share_value = this.share.value;
        let trade_datas = {};
        trade_datas.strat_name = this.tab_strat[this.strategy.selectedIndex].name;
        trade_datas.option = this.option[this.option.selectedIndex].value;
        trade_datas.date_start = actionObjet.date_start;
        trade_datas.date_end = actionObjet.date_end;
        trade_datas.ticker_id = actionObjet.ticker_id;
        trade_datas.share = share_value;
        trade_datas.stop_loss = stop_loss_value;
        this.socket.emit('run_strat',trade_datas);
    }
}