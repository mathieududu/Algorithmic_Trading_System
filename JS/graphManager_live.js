/**
 * Created by mathieuduverney on 07/02/2017.
 */
/**
 * Created by leletir on 19/12/2016.
 */
class graphManager_live {
    //Constructor
    constructor(socket) {
        this.chartLinear;
        this.chartBar;
        this.chartCandle;
        this.daily_price_candle = [];
        this.linear = [];
        this.candle = [];
        this.ticker_name = "";
        this.socket = socket;
        this.which_one = undefined;
        this.id = undefined;
        this.socket.on('data_chart', (function (data) {
            this.linear = data.linear;
            this.candle = data.candle;
            this.constructDataForCandle();
            this.whereIGo();
        }).bind(this));
        this.socket.on('ticker_name', (function (data) {
            this.ticker_name = data;
        }).bind(this));
    }





    constructDataForCandle() //on doit concaténer les deux tableaux pour tracer le candle
    {
        for(var i = 0; i< this.linear.length; ++i)
        {
            this.daily_price_candle[i] = [this.linear[i][0],this.candle[i][0],this.candle[i][1],this.candle[i][2],this.linear[i][1]]

        }
    }

    whereIGo()
    {
        if(this.which_one == 1)
        {
            this.drawLinear(this.linear,this.ticker_name);
        }
        else if(this.which_one == 2)
        {
            this.drawCandleStick(this.daily_price_candle,this.ticker_name);
        }
    }




    drawLinear(tab_data,ticker_name)
    {
        console.log(ticker_name);
        actionObjet.setClose(tab_data);
        $(document).ready(function () {
            Highcharts.setOptions({
                global: {
                    useUTC: false
                }
            });

            var last;
            var volume;

// Create the chart

            Highcharts.stockChart('container_live', {
                chart: {
                    events: {
                        load: function () {

                            // set up the updating of the chart each second
                            var seriesD = this.series[0];
                            console.log(seriesD);
                            setInterval((function () {
                                //var token='802d38c227e92151e63388be12d40e76';
                                var token = 'b46abd262082092e72e44e673bbcac51';
                                var request = 'https://marketdata.websol.barchart.com/getQuote.json?key=' + token + '&symbols=' + ticker_name;
                                var xmlHttp = new XMLHttpRequest();
                                xmlHttp.onreadystatechange = function () {
                                    if (xmlHttp.readyState == 4 && xmlHttp.status == 200) {//console.log(JSON.parse(xmlHttp.responseText));
                                        let dataReceived = JSON.parse(xmlHttp.responseText).results[0];
                                        last = dataReceived.lastPrice;
                                        volume = dataReceived.volume;
                                        var dateR = (new Date()).getTime();
                                        seriesD.addPoint([dateR, last], true, false);
                                    }
                                };
                                xmlHttp.open("GET", request, true); // true for asynchronous
                                xmlHttp.setRequestHeader('Access-Control-Allow-Headers', 'application/json');
                                xmlHttp.setRequestHeader('Accept', 'application/json');
                                xmlHttp.setRequestHeader('Content-Type', 'application/json');
                                xmlHttp.setRequestHeader('Access-Control-Allow-Methods', 'GET');
                                xmlHttp.setRequestHeader('Access-Control-Allow-Origin', '*');

                                xmlHttp.send(null);

                            }), 2000);
                        }
                    }
                },

                rangeSelector: {
                    buttons: [{
                        count: 1,
                        type: 'minute',
                        text: '1M'
                    }, {
                        count: 5,
                        type: 'minute',
                        text: '5M'
                    }, {
                        type: 'all',
                        text: 'All'
                    }],
                    inputEnabled: false,
                    selected: 0
                },

                title: {
                    text: ticker_name
                },

                exporting: {
                    enabled: true
                },

                series: [{
                    name: ticker_name,
                    type: 'area',
                    illColor: {
                        linearGradient: {
                            x1: 0,
                            y1: 0,
                            x2: 0,
                            y2: 1
                        },
                        stops: [
                            [0, Highcharts.getOptions().colors[0]],
                            [1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
                        ]
                    },
                    data: []
                }]
            });

        });

        Highcharts.createElement('link', {
            href: 'https://fonts.googleapis.com/css?family=Unica+One',
            rel: 'stylesheet',
            type: 'text/css'
        }, null, document.getElementsByTagName('head')[0]);

        Highcharts.theme = {
            colors: ['#3b9cdd', '#90ee7e', '#f45b5b', '#7798BF', '#aaeeee', '#ff0066', '#eeaaee',
                '#55BF3B', '#DF5353', '#7798BF', '#aaeeee'],
            chart: {
                backgroundColor: {
                    linearGradient: { x1: 0, y1: 0, x2: 1, y2: 1 },
                    stops: [
                        [0, '#2a2a2b'],
                        [1, '#3e3e40']
                    ]
                },
                style: {
                    fontFamily: '\'Unica One\', sans-serif'
                },
                plotBorderColor: '#606063'
            },
            title: {
                style: {
                    color: '#E0E0E3',
                    textTransform: 'uppercase',
                    fontSize: '20px'
                }
            },
            subtitle: {
                style: {
                    color: '#E0E0E3',
                    textTransform: 'uppercase'
                }
            },
            xAxis: {
                gridLineColor: '#707073',
                labels: {
                    style: {
                        color: '#E0E0E3'
                    }
                },
                lineColor: '#707073',
                minorGridLineColor: '#505053',
                tickColor: '#707073',
                title: {
                    style: {
                        color: '#A0A0A3'

                    }
                }
            },
            yAxis: {
                gridLineColor: '#707073',
                labels: {
                    style: {
                        color: '#E0E0E3'
                    }
                },
                lineColor: '#707073',
                minorGridLineColor: '#505053',
                tickColor: '#707073',
                tickWidth: 1,
                title: {
                    style: {
                        color: '#A0A0A3'
                    }
                }
            },
            tooltip: {
                backgroundColor: 'rgba(0, 0, 0, 0.85)',
                style: {
                    color: '#F0F0F0'
                }
            },
            plotOptions: {
                series: {
                    dataLabels: {
                        color: '#B0B0B3'
                    },
                    marker: {
                        lineColor: '#333'
                    }
                },
                boxplot: {
                    fillColor: '#505053'
                },
                candlestick: {
                    lineColor: 'white'
                },
                errorbar: {
                    color: 'white'
                }
            },
            legend: {
                itemStyle: {
                    color: '#E0E0E3'
                },
                itemHoverStyle: {
                    color: '#FFF'
                },
                itemHiddenStyle: {
                    color: '#606063'
                }
            },
            credits: {
                style: {
                    color: '#666'
                }
            },
            labels: {
                style: {
                    color: '#707073'
                }
            },

            drilldown: {
                activeAxisLabelStyle: {
                    color: '#F0F0F3'
                },
                activeDataLabelStyle: {
                    color: '#F0F0F3'
                }
            },

            navigation: {
                buttonOptions: {
                    symbolStroke: '#DDDDDD',
                    theme: {
                        fill: '#505053'
                    }
                }
            },

            // scroll charts
            rangeSelector: {
                buttonTheme: {
                    fill: '#505053',
                    stroke: '#000000',
                    style: {
                        color: '#CCC'
                    },
                    states: {
                        hover: {
                            fill: '#707073',
                            stroke: '#000000',
                            style: {
                                color: 'white'
                            }
                        },
                        select: {
                            fill: '#000003',
                            stroke: '#000000',
                            style: {
                                color: 'white'
                            }
                        }
                    }
                },
                inputBoxBorderColor: '#505053',
                inputStyle: {
                    backgroundColor: '#333',
                    color: 'silver'
                },
                labelStyle: {
                    color: 'silver'
                }
            },

            navigator: {
                handles: {
                    backgroundColor: '#666',
                    borderColor: '#AAA'
                },
                outlineColor: '#CCC',
                maskFill: 'rgba(255,255,255,0.1)',
                series: {
                    color: '#7798BF',
                    lineColor: '#A6C7ED'
                },
                xAxis: {
                    gridLineColor: '#505053'
                }
            },

            scrollbar: {
                barBackgroundColor: '#808083',
                barBorderColor: '#808083',
                buttonArrowColor: '#CCC',
                buttonBackgroundColor: '#606063',
                buttonBorderColor: '#606063',
                rifleColor: '#FFF',
                trackBackgroundColor: '#404043',
                trackBorderColor: '#404043'
            },

            // special colors for some of the
            legendBackgroundColor: 'rgba(0, 0, 0, 0.5)',
            background2: '#505053',
            dataLabelsColor: '#B0B0B3',
            textColor: '#C0C0C0',
            contrastTextColor: '#F0F0F3',
            maskColor: 'rgba(255,255,255,0.3)'
        };

// Apply the theme
        Highcharts.setOptions(Highcharts.theme);

        $(document).ready(function() {
            this.chart = new Highcharts.stockChart('container_live');
            console.log(this.chart);
        });
    }
}