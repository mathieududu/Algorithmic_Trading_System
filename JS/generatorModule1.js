/**
 * Created by leletir on 26/12/2016.
 */
class generatorModule1 {
    constructor(data) {
        //console.log(data);
        this.marketTrust = [];
        this.tableauTrust = [];
        this.div_Global = [];
        this.div=[];

        this.marketTrust = this.sort_country(data);
        this.tableauTrust = this.create_tableau_bien(this.marketTrust);
        this.create_balise(this.tableauTrust,this.div,this.div_Global);

    }


    sort_country(data) {
        let data_array = data.market;

        //trie les données par rapport au pays
        data_array.sort(function (a, b) {
            return a[0] > b[0]
        });

        return data_array;
    }

    create_tableau_bien(data) {

        let tableau_final = [];

        var j = 0;
        var k = 0;

        for (var i in data) {
            if (i == 0) {
                tableau_final.push([data[i][0], [data[i][1]]]);
            }
            else if (data[i][0] == tableau_final[j][0]) {
                tableau_final[j][1].push(data[i][1]);
            }
            else {
                tableau_final.push([data[i][0], [data[i][1]]]);
                j++;
            }
        }
        //console.log(tableau_final);
        return tableau_final;
    }


    create_balise(data,div,divGlobal) {

        divGlobal = document.querySelector('#actionName');

        for (var i = 0; i < data.length; ++i) {

            let div_container = document.createElement('div');
            div_container.className= 'div_container_pays';

            let div_flag = document.createElement('div');
            div_flag.className = 'div_container_infoPays';



            let img = document.createElement('img');
            img.src = 'img/'+data[i][0]+'.png';
            img.style.height = "23px";
            img.style.width = "23px";
            div_flag.appendChild(img);

            let div_nomPays = document.createElement('div');
            div_nomPays.textContent = data[i][0];
            div_flag.appendChild(div_nomPays);

            div_container.appendChild(div_flag);

            let div_pays = document.createElement('div');
            div_pays.className = 'divpays';

            div_container.appendChild(div_pays);



            for (var j = 0; j < data[i][1].length; ++j) {

                let test_bouton = 0; //boolean pour ouvrir fermer les lettres

                //on crée le div qui contiendra les div des lettres
                let div_lettre = document.createElement('div');
                div_lettre.className ='div_lettres';
                div_lettre.style.display = 'none'; // on le cache
                div_lettre.onclick = (function () {
                    actionObjet.cleanAll();
                    actionObjet.setMarketName(this.market);
                    actionObjet.setCurrency(this.currency);
                    actionObjet.setTimeZone(this.timeZone, this.id);
                    // this.socket.emit('sendIdMarket', this.id);
                }).bind({
                    market: data[i][1][j][3],
                    id: data[i][1][j][4],
                    //socket: socket,
                    currency: data[i][1][j][0],
                    timeZone: data[i][1][j][1]
                });

                //on crée le bouton contenant le marché
                let div_marche = document.createElement('button');
                div_marche.textContent = data[i][1][j][3];
                div_marche.className = 'button_market';
                div_marche.onclick = (function () {

                    if (test_bouton == 0) {
                        this.dl.style.display = "block";
                        test_bouton = 1;
                    }
                    else if (test_bouton == 1) {
                        this.dl.style.display = "none";
                        test_bouton = 0;
                    }

                }).bind({dl: div_lettre});

                //on l'insère dans le div pays
                div_pays.appendChild(div_marche);
                //et on l'insere à la suite du bouton
                div_pays.insertBefore(div_lettre, div_marche.nextSibling);

                //////////////////////////////////////////////////////////////

                // on crée les lettres AH
                let div_lettre_AH = document.createElement('div');
                div_lettre_AH.textContent = 'A-G';
                div_lettre_AH.id = data[i][1][j][4];
                div_lettre_AH.className = 'lettre';
                div_lettre_AH.onclick = (function () {
                    sendId(this.lettreAG, 'A-G');
                }).bind({lettreAG: data[i][1][j][4]});

                //qu'on insère dans le div_lettre
                div_lettre.appendChild(div_lettre_AH);

                // on crée les lettres HR
                let div_lettre_HR = document.createElement('div');
                div_lettre_HR.textContent = 'H-R';
                div_lettre_HR.id = data[i][1][j][4];
                div_lettre_HR.className = 'lettre';
                div_lettre_HR.onclick = (function () {
                    sendId(this.lettreHR, 'H-R');
                }).bind({lettreHR: data[i][1][j][4]});


                //qu'on insère dans le div_lettre
                div_lettre.appendChild(div_lettre_HR);

                // on crée les lettres RZ
                let div_lettre_RZ = document.createElement('div');
                div_lettre_RZ.textContent = 'S-Z';
                div_lettre_RZ.id = data[i][1][j][4];
                div_lettre_RZ.className = 'lettre';
                div_lettre_RZ.onclick = (function () {
                    sendId(this.lettreSZ, 'S-Z');
                }).bind({lettreSZ: data[i][1][j][4]});


                //qu'on insère dans le div_lettre
                div_lettre.appendChild(div_lettre_RZ);


            }

            // on insère tout dans le div global
            divGlobal.appendChild(div_container);


        }
    }
}



function afficheAction(){

    displayTicker = [];
    var premiereAction = 0;//premiere action à afficher
    var derniereAction = 0;//derniere action à afficher


    for (var i = 0; i < action_List_Triee_Ticker.length; i++) {


        if (lettre_selectionner_en_cours == "A-G") {

            if (action_List_Triee_Ticker[i] < "H") {
                derniereAction++
            }
        }
        if (lettre_selectionner_en_cours == "H-R") {
            if (action_List_Triee_Ticker[i] <= "H") {
                premiereAction++
            }
            if (action_List_Triee_Ticker[i] < "R") {
                derniereAction++
            }
        }
        if (lettre_selectionner_en_cours == "S-Z") {
            if (action_List_Triee_Ticker[i] <= "R") {
                premiereAction++
            }
            derniereAction = action_List_Triee_Ticker.length;
        }
        else console.log("erreur, pas d'intervale de lettre en memoire")
    }


    for (var i = premiereAction; i < derniereAction; i++) {

        //on affiche les tickers suivi du old et du new

        displayTicker[i] = generator(id_ticker[i], graphManagerObject, ticker[i], old_price[i], new_price[i], socket);
        //displayTicker[i] = '<p><button id='+id_ticker[i]+' class="contenupetit" onClick="graphManagerObject.drawGraph(1,this.id_ticker[i])">' + ticker[i] + '</button>' + '<span class="margin">' + old_price[i] + '</span><span class="margin" >' + new_price[i]+'</span></p>';

    }


    var module2 = document.getElementById('ValuesAction');
    module2.innerHTML = '';
    displayTicker.forEach((function (elt) {
        this.appendChild(elt);
    }).bind(module2));


}


function sendId(id,lettre){

    lettre_selectionner_en_cours = lettre; //on defini quel intervalle de lettre on veut
    //on ne l'envoi pas dans la requete car on va s'en servire uniquement pour definir les actions
    // que l'on affiche coté client

    if(id == id_market_en_memoire){

        this.afficheAction();

    } else {
        console.log(id);
    }

    id_market_en_memoire = id;

    socket.emit('sendIdMarket',id);


}
