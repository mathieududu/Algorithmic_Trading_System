/**
 * Created by leletir on 20/12/2016.
 */

class windowsManager {
    constructor(){
        this.height = window.innerHeight;
        this.width = window.innerWidth;
        this.modules = document.querySelector('#modules');
        this.orders = document.querySelector('#orders');
        this.modules.style.height = this.height*(0.7)+'px';
        this.orders.style.height = this.height*(0.2)+'px';

        this.div_cache = document.querySelector('#cache_trade');

        window.addEventListener('resize',(function(){
            this.height = window.innerHeight;
            this.width = window.innerWidth;
            this.modules.style.height = this.height*(0.7)+'px';
            this.orders.style.height = this.height*(0.2)+'px';
            /*alert("Please play the full screen mode !");*/

            //Gestion cache
            this.div_cache.style.width = this.width+'px';
            this.div_cache.style.height = this.height+'px';
        }).bind(this));

        //Gestion ticket

        this.div_cache.style.width = this.width+'px';
        this.div_cache.style.height = this.height+'px';
    }
}