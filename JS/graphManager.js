/**
 * Created by leletir on 19/12/2016.
 */
class graphManager {
    //Constructor
    constructor(socket){
        this.chartLinear;
        this.chartBar;
        this.chartCandle;
        this.buttonNormal = document.querySelector('#buttonNormal');
        this.buttonCandle = document.querySelector('#buttonCandle');
        this.buttonBarChart = document.querySelector('#buttonBarChart');
        this.daily_price_candle = [];
        this.linear = [];
        this.candle = [];
        this.ticker_name="";
        this.socket=socket;
        this.which_one = undefined;
        this.id = undefined;
        this.socket.on('data_chart',(function (data) {
            this.linear=data.linear;
            this.candle=data.candle;
            this.constructDataForCandle();
            this.whereIGo();
        }).bind(this));
        this.socket.on('ticker_name',(function (data){
            this.ticker_name = data;
        }).bind(this));

        this.buttonNormal.onclick = (function(){
            this.buttonNormal.style.backgroundColor = '#333333';
            this.buttonNormal.style.color = "#3b9cdd";

            this.buttonBarChart.style.backgroundColor = '#444444';
            this.buttonBarChart.style.color = "#fff";

            this.buttonCandle.style.backgroundColor = '#444444';
            this.buttonCandle.style.color = "#fff";
            this.drawLinear(this.linear, this.ticker_name);
        }).bind(this);

        this.buttonCandle.onclick = (function(){
            this.drawCandleStick(this.daily_price_candle, this.ticker_name);
            this.buttonNormal.style.backgroundColor = '#444444';
            this.buttonNormal.style.color = "#fff";

            this.buttonBarChart.style.backgroundColor = '#444444';
            this.buttonBarChart.style.color = "#fff";

            this.buttonCandle.style.backgroundColor = '#333333';
            this.buttonCandle.style.color = "#3b9cdd";

        }).bind(this);

        this.buttonBarChart.onclick = (function(){
            this.drawBarChart(this.daily_price_candle, this.ticker_name);
            this.buttonNormal.style.backgroundColor = '#444444';
            this.buttonNormal.style.color = "#fff";

            this.buttonBarChart.style.backgroundColor = '#333333';
            this.buttonBarChart.style.color = "#3b9cdd";

            this.buttonCandle.style.backgroundColor = '#444444';
            this.buttonCandle.style.color = "#fff";

        }).bind(this);
    }


    searchInDaily_price_candle(daily_price_candle,date)
    {
        return daily_price_candle.find((function(elt){
            return elt[0] == this;
        }).bind(date));
    }

    constructDataForCandle() //on doit concaténer les deux tableaux pour tracer le candle
    {
        for(var i = 0; i< this.linear.length; ++i)
        {
            this.daily_price_candle[i] = [this.linear[i][0],this.candle[i][0],this.candle[i][1],this.candle[i][2],this.linear[i][1]]

        }
    }

    whereIGo()
    {
        if(this.which_one == 1)
        {
            this.drawLinear(this.linear,this.ticker_name);
        }
        else if(this.which_one == 2)
        {
            this.drawCandleStick(this.daily_price_candle,this.ticker_name);
        }
    }

    drawGraph(which_one, id_ticker)
    {
        this.which_one = which_one; // sauv
        //1er truc comparer id avec this.id
        if((this.id != id_ticker) || (this.linear.length == 0) || (this.daily_price_candle.length == 0))
        {
            this.id = id_ticker; //sauv
            this.socket.emit('sendIdTicker',id_ticker);
            this.buttonNormal.style.visibility = 'visible';
            this.buttonCandle.style.visibility = 'visible';
        }
        else
        {
            this.whereIGo();
        }
    }

    drawLinear(tab_data,ticker_name)
    {
        actionObjet.setClose(tab_data);
        var options = {

            rangeSelector: {
                selected: 4
            },


            yAxis: {

                labels: {
                    formatter: function () {
                        return this.value ;
                    }
                },
                plotLines: [{
                    value: 0,
                    width: 2,
                    color: 'silver'
                }]
            },



            tooltip: {
                pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b> ({point.change}%)<br/>',
                valueDecimals: 2,
                split: true
            },

            series: [{
                name: ticker_name,
                data: tab_data,
                type:'area',
                fillColor: {
                    linearGradient: {
                        x1: 0,
                        y1: 0,
                        x2: 0,
                        y2: 1
                    },
                    stops: [
                        [0, Highcharts.getOptions().colors[0]],
                        [1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
                    ]
                },
                id: 'dataseries',
             //   compare: 'percent',
                showInNavigator: true,
                cursor:'pointer',
                point:{
                    events:{
                        click: (function(e){
                            //console.log(e.point.x);
                            var eventManagerGraphModule3InfoObjet = new eventManagerGraphModule3Info(this.id, this.socket);
                            eventManagerGraphModule3InfoObjet.eventManagerLinear(this.searchInDaily_price_candle(this.daily_price_candle,e.point.x));
                        }).bind(this)
                    }
                }
            }
            ]
        };

        Highcharts.createElement('link', {
            href: 'https://fonts.googleapis.com/css?family=Unica+One',
            rel: 'stylesheet',
            type: 'text/css'
        }, null, document.getElementsByTagName('head')[0]);

        Highcharts.theme = {
            colors: ['#3b9cdd', '#90ee7e', '#f45b5b', '#7798BF', '#aaeeee', '#ff0066', '#eeaaee',
                '#55BF3B', '#DF5353', '#7798BF', '#aaeeee'],

            chart: {
                backgroundColor: {
                    linearGradient: [0, 0, 500, 500],
                    stops: [
                        [0, 'rgb(51, 51, 51)'],
                        [1, 'rgb(51, 51, 51)']
                    ]
                },



                style: {
                    fontFamily: '\'Unica One\', sans-serif'
                },
                plotBorderColor: '#606063'
            },
            title: {
                style: {
                    color: '#E0E0E3',
                    textTransform: 'uppercase',
                    fontSize: '20px'
                }
            },
            subtitle: {
                style: {
                    color: '#E0E0E3',
                    textTransform: 'uppercase'
                }
            },
            xAxis: {
                gridLineColor: '#707073',
                labels: {
                    style: {
                        color: '#E0E0E3'
                    }
                },
                lineColor: '#707073',
                minorGridLineColor: '#505053',
                tickColor: '#707073',
                title: {
                    style: {
                        color: '#A0A0A3'

                    }
                }
            },
            yAxis: {
                gridLineColor: '#707073',
                labels: {
                    style: {
                        color: '#E0E0E3'
                    }
                },
                lineColor: '#707073',
                minorGridLineColor: '#505053',
                tickColor: '#707073',
                tickWidth: 1,
                title: {
                    style: {
                        color: '#A0A0A3'
                    }
                }
            },
            tooltip: {
                backgroundColor: 'rgba(0, 0, 0, 0.85)',
                style: {
                    color: '#F0F0F0'
                }
            },
            plotOptions: {
                series: {
                    dataLabels: {
                        color: '#B0B0B3'
                    },
                    marker: {
                        lineColor: '#333'
                    }
                },
                boxplot: {
                    fillColor: '#505053'
                },
                candlestick: {
                    lineColor: 'white'
                },
                errorbar: {
                    color: 'white'
                }

            },
            legend: {
                itemStyle: {
                    color: '#E0E0E3'
                },
                itemHoverStyle: {
                    color: '#FFF'
                },
                itemHiddenStyle: {
                    color: '#606063'
                }
            },
            credits: {
                style: {
                    color: '#666'
                }
            },
            labels: {
                style: {
                    color: '#707073'
                }
            },

            drilldown: {
                activeAxisLabelStyle: {
                    color: '#F0F0F3'
                },
                activeDataLabelStyle: {
                    color: '#F0F0F3'
                }
            },

            navigation: {
                buttonOptions: {
                    symbolStroke: '#DDDDDD',
                    theme: {
                        fill: '#505053'
                    }
                }
            },

            // scroll charts
            rangeSelector: {
                buttonTheme: {
                    fill: '#505053',
                    stroke: '#000000',
                    style: {
                        color: '#CCC'
                    },
                    states: {
                        hover: {
                            fill: '#707073',
                            stroke: '#000000',
                            style: {
                                color: 'white'
                            }
                        },
                        select: {
                            fill: '#000003',
                            stroke: '#000000',
                            style: {
                                color: 'white'
                            }
                        }
                    }
                },
                inputBoxBorderColor: '#505053',
                inputStyle: {
                    backgroundColor: '#333',
                    color: 'silver'
                },
                labelStyle: {
                    color: 'silver'
                }
            },

            navigator: {
                handles: {
                    backgroundColor: '#666',
                    borderColor: '#AAA'
                },
                outlineColor: '#CCC',
                maskFill: 'rgba(255,255,255,0.1)',
                series: {
                    color: '#7798BF',
                    lineColor: '#A6C7ED'
                },
                xAxis: {
                    gridLineColor: '#505053'
                }
            },

            scrollbar: {
                barBackgroundColor: '#808083',
                barBorderColor: '#808083',
                buttonArrowColor: '#CCC',
                buttonBackgroundColor: '#606063',
                buttonBorderColor: '#606063',
                rifleColor: '#FFF',
                trackBackgroundColor: '#404043',
                trackBorderColor: '#404043'
            },

            // special colors for some of the
            legendBackgroundColor: 'rgba(0, 0, 0, 0.5)',
            background2: '#505053',
            dataLabelsColor: '#B0B0B3',
            textColor: '#C0C0C0',
            contrastTextColor: '#F0F0F3',
            maskColor: 'rgba(255,255,255,0.3)'
        };

// Apply the theme
        Highcharts.setOptions(Highcharts.theme);

        $(document).ready(function() {
            this.chart = new Highcharts.stockChart('container',options);
            console.log(this.chart);
        });
    }

    drawCandleStick(tab_data,ticker_name)
    {
        actionObjet.setClose(tab_data);
        var options = {
            rangeSelector: {
                selected: 0
            },


            series: [{
                type: 'candlestick',
                name: ticker_name,
                data: tab_data,
                dataGrouping: {
                    units: [[
                        'day', // unit name
                        [1] // allowed multiples
                    ],[
                        'year',
                        [1,2,3,4,5,6]
                    ]]
                },
                cursor:'pointer',
                point:{
                    events:{
                        click: (function(e){
                            var eventManagerGraphModule3InfoObjet = new eventManagerGraphModule3Info(this.id, this.socket);
                            eventManagerGraphModule3InfoObjet.eventManagerCandle(e.point.open,e.point.close,e.point.high,e.point.low,e.point.x);
                        }).bind(this)
                    }
                }
            }]
        };

        Highcharts.createElement('link', {
            href: 'https://fonts.googleapis.com/css?family=Unica+One',
            rel: 'stylesheet',
            type: 'text/css'
        }, null, document.getElementsByTagName('head')[0]);

        Highcharts.theme = {
            colors: ['#3b9cdd', '#90ee7e', '#f45b5b', '#7798BF', '#aaeeee', '#ff0066', '#eeaaee',
                '#55BF3B', '#DF5353', '#7798BF', '#aaeeee'],
            chart: {
                backgroundColor: {
                    linearGradient: { x1: 0, y1: 0, x2: 1, y2: 1 },
                    stops: [
                        [0, '#2a2a2b'],
                        [1, '#3e3e40']
                    ]
                },
                style: {
                    fontFamily: '\'Unica One\', sans-serif'
                },
                plotBorderColor: '#606063'
            },
            title: {
                style: {
                    color: '#E0E0E3',
                    textTransform: 'uppercase',
                    fontSize: '20px'
                }
            },
            subtitle: {
                style: {
                    color: '#E0E0E3',
                    textTransform: 'uppercase'
                }
            },
            xAxis: {
                gridLineColor: '#707073',
                labels: {
                    style: {
                        color: '#E0E0E3'
                    }
                },
                lineColor: '#707073',
                minorGridLineColor: '#505053',
                tickColor: '#707073',
                title: {
                    style: {
                        color: '#A0A0A3'

                    }
                }
            },
            yAxis: {
                gridLineColor: '#707073',
                labels: {
                    style: {
                        color: '#E0E0E3'
                    }
                },
                lineColor: '#707073',
                minorGridLineColor: '#505053',
                tickColor: '#707073',
                tickWidth: 1,
                title: {
                    style: {
                        color: '#A0A0A3'
                    }
                }
            },
            tooltip: {
                backgroundColor: 'rgba(0, 0, 0, 0.85)',
                style: {
                    color: '#F0F0F0'
                }
            },
            plotOptions: {
                series: {
                    dataLabels: {
                        color: '#B0B0B3'
                    },
                    marker: {
                        lineColor: '#333'
                    }
                },
                boxplot: {
                    fillColor: '#505053'
                },
                candlestick: {
                    lineColor: 'red',
                    upColor:'green',
                    upLineColor:"green",
                    color:'#FF0000'

                },
                errorbar: {
                    color: 'white'
                }
            },
            legend: {
                itemStyle: {
                    color: '#E0E0E3'
                },
                itemHoverStyle: {
                    color: '#FFF'
                },
                itemHiddenStyle: {
                    color: '#606063'
                }
            },
            credits: {
                style: {
                    color: '#666'
                }
            },
            labels: {
                style: {
                    color: '#707073'
                }
            },

            drilldown: {
                activeAxisLabelStyle: {
                    color: '#F0F0F3'
                },
                activeDataLabelStyle: {
                    color: '#F0F0F3'
                }
            },

            navigation: {
                buttonOptions: {
                    symbolStroke: '#DDDDDD',
                    theme: {
                        fill: '#505053'
                    }
                }
            },

            // scroll charts
            rangeSelector: {
                buttonTheme: {
                    fill: '#505053',
                    stroke: '#000000',
                    style: {
                        color: '#CCC'
                    },
                    states: {
                        hover: {
                            fill: '#707073',
                            stroke: '#000000',
                            style: {
                                color: 'white'
                            }
                        },
                        select: {
                            fill: '#000003',
                            stroke: '#000000',
                            style: {
                                color: 'white'
                            }
                        }
                    }
                },
                inputBoxBorderColor: '#505053',
                inputStyle: {
                    backgroundColor: '#333',
                    color: 'silver'
                },
                labelStyle: {
                    color: 'silver'
                }
            },

            navigator: {
                handles: {
                    backgroundColor: '#666',
                    borderColor: '#AAA'
                },
                outlineColor: '#CCC',
                maskFill: 'rgba(255,255,255,0.1)',
                series: {
                    color: '#7798BF',
                    lineColor: '#A6C7ED'
                },
                xAxis: {
                    gridLineColor: '#505053'
                }
            },

            scrollbar: {
                barBackgroundColor: '#808083',
                barBorderColor: '#808083',
                buttonArrowColor: '#CCC',
                buttonBackgroundColor: '#606063',
                buttonBorderColor: '#606063',
                rifleColor: '#FFF',
                trackBackgroundColor: '#404043',
                trackBorderColor: '#404043'
            },

            // special colors for some of the
            legendBackgroundColor: 'rgba(0, 0, 0, 0.5)',
            background2: '#505053',
            dataLabelsColor: '#B0B0B3',
            textColor: '#C0C0C0',
            contrastTextColor: '#F0F0F3',
            maskColor: 'rgba(255,255,255,0.3)'
        };

// Apply the theme
        Highcharts.setOptions(Highcharts.theme);
        $(document).ready(function() {
            this.chartCandle = new Highcharts.stockChart('container',options);
        });
    }

    drawBarChart(tab_data,ticker_name) {
        actionObjet.setClose(tab_data);
        var options = {

            rangeSelector: {
                selected: 2
            },


            series: [{
                type: 'ohlc',
                name: ticker_name,
                data: tab_data,
                dataGrouping: {
                    units: [[
                        'week', // unit name
                        [1] // allowed multiples
                    ], [
                        'month',
                        [1, 2, 3, 4, 6]
                    ]]
                },
                point:{
                    events:{
                        click: (function(e){
                            var eventManagerGraphModule3InfoObjet = new eventManagerGraphModule3Info(this.id, this.socket);
                            eventManagerGraphModule3InfoObjet.eventManagerCandle(e.point.open,e.point.close,e.point.high,e.point.low,e.point.x);
                        }).bind(this)
                    }
                }
            }]

        };

        $(document).ready(function() {
            this.chartBar = new Highcharts.stockChart('container',options);
        });

    }

    add_Series(data_buy_sell){
        console.log(data_buy_sell);

     var flag_Series = $('#container').highcharts();
     console.log(flag_Series);
     flag_Series.addSeries({name: 'Events',
         data: data_buy_sell,
         type: 'flags',
         onSeries:'dataseries',
         shape: 'circlepin',
         width: 8,
         style: { // text style
             color: 'white'
         },
     }, true, false)
    }
}
