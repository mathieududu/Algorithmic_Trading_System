/**
 * Created by leletir on 27/12/2016.
 */
class strategy {
    constructor(socket) {
        this.retourStrat =[];
        this.divGlobal= '';
        this.socket = socket;
        this.divGlobal = document.querySelector('#orders');
        /*this.divGlobal= '';*/
        this.divWrap = document.querySelector('#orderswrap');
        /*this.divWrap.textContent = '';*/
        this.share = document.querySelector('#shares_ticket');
        console.log(this.divGlobal);
        this.socket.on('Python_Response',(function (data) {

            var childNb = document.getElementById("orderswrap").childNodes.length;
            if (childNb >= 9)
            {
                $('.div_order').remove();
                $('.orders-info').remove();
            }
            
            this.retourPython = JSON.parse(data);
            console.log(this.retourPython);
            for(var i = 0; i< (this.retourPython.length - 8); ++i) //-8 pour pas récupérer les infos globale
            {
                let div_order = document.createElement('div');
                div_order.className = 'div_order';
                //div_order.textContent = this.retourPython[i].order["Date"];

                let img = document.createElement('img');
                img.src = 'img/equity.png';
                img.style.height = "20px";
                img.style.width = "20px";
                img.className ='img_sizing_order';
                div_order.appendChild(img);


                let div_date = document.createElement('div');
                div_date.className ='div_order_date';
                div_date.textContent = this.retourPython[i].order["Date"];
                div_order.appendChild(div_date);

                let div_name = document.createElement('div');
                div_name.className ='div_order_name';
                div_name.textContent = actionObjet.ticker_name;
                div_order.appendChild(div_name);

                let div_operation = document.createElement('div');
                div_operation.className = 'div_order_operation';

                if(this.retourPython[i].order["Operation"] == 0)
                {
                    div_operation.textContent = '.BUY';
                    this.retourStrat.push({"x": new Date(this.retourPython[i].order["Date"]).getTime(), "title": "B","color":"green","fillColor":"green"});
                }
                else
                {
                    div_operation.textContent = 'SELL';
                    this.retourStrat.push({"x": new Date(this.retourPython[i].order["Date"]).getTime(),"title": 'S',"color":"red","fillColor":"red"});
                }
                div_order.appendChild(div_operation);

                let div_price = document.createElement('div');
                div_price.className = 'div_order_price';
                div_price.textContent = '$'+Math.round(this.retourPython[i].order["Price"]).toFixed(4);
                div_order.appendChild(div_price);

                let div_share = document.createElement('div');
                div_share.className = 'div_order_share';
                div_share.textContent = this.share.value +' shares';
                div_order.appendChild(div_share);

                let div_fee = document.createElement('div');
                div_fee.className = 'div_order_fee';
                div_fee.textContent = 'Fee: '+this.retourPython[i].order["Fee"];
                div_order.appendChild(div_fee);
                this.divGlobal.appendChild(div_order);
            }

                let orders_title = document.createElement('div');


                orders_title.className = 'orders-info';

                orders_title.textContent = 'Initial: '+this.retourPython[this.retourPython.length - 8]["initial"];
                orders_title.textContent += ' Final: '+Math.round(this.retourPython[this.retourPython.length - 7]["final"]).toFixed(4);
                orders_title.textContent += ' Gain: '+Math.round(this.retourPython[this.retourPython.length - 6]["gain"]).toFixed(4);
                orders_title.textContent += ' Percentage: '+Math.round(this.retourPython[this.retourPython.length - 5]["percentage"]).toFixed(4);
                orders_title.textContent += ' Annual Return: '+Math.round(this.retourPython[this.retourPython.length - 4]["Annual_return"]).toFixed(4);
                orders_title.textContent += ' Daily Return: '+Math.round(this.retourPython[this.retourPython.length - 3]["daily_return"]).toFixed(4);
                orders_title.textContent += ' Std_dev: '+Math.round(this.retourPython[this.retourPython.length - 2]["Std_dev"]).toFixed(4);
                orders_title.textContent += ' Sharpe: '+Math.round(this.retourPython[this.retourPython.length - 1]["sharpe"]).toFixed(4);

                this.divWrap.appendChild(orders_title);

            graphManagerObject.add_Series(this.retourStrat);
            this.retourStrat=[];
            //graphManager.add_Series(this.retourStrat);
        }).bind(this));

    }

}