/**
 * Created by leletir on 22/12/2016.
 */
class changePage {
    //Constructor
    constructor() {
        this.port = '50022';
        this.buttonBackTesting = document.querySelector('#buttonBackTesting');
        this.buttonTrading = document.querySelector('#buttonTrading');
        this.buttonNews = document.querySelector('#buttonNews');

        this.buttonBackTesting.onclick = (function () {
            window.location = "https://finance-ats.ddns.net:"+this.port+"/back";
        }).bind(this);

        this.buttonTrading.onclick = (function () {
            window.location = "https://finance-ats.ddns.net:"+this.port+"/live";
        }).bind(this);

        this.buttonNews.onclick = (function () {
            window.location = "https://finance-ats.ddns.net:"+this.port+"/news";
        }).bind(this);

    }

}