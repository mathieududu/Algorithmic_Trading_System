/**
 * Created by leletir on 21/12/2016.
 */
class eventManagerGraphModule3Info {
    constructor(id ,socket) {
        this.p_date = document.querySelector('#p_date');
        this.p_change = document.querySelector('#p_change');
        this.p_day_range = document.querySelector('#p_day_range');
        this.p_high = document.querySelector('#p_high');
        this.p_low = document.querySelector('#p_low');
        this.p_open = document.querySelector('#p_open');
        this.p_close = document.querySelector('#p_close');
        this.p_volume = document.querySelector('#p_volume');
        this.id = id;
        this.socket = socket;
        this.socket.on('retour_volume_info',(function (data) {
            this.p_volume.textContent = data.volume;
        }).bind(this));
    }

    eventManagerCandle(open,close,high,low,date)
    {
        var date1 = moment(date).format("YYYY-MM-DD");
        this.updateVolume(this.id,date1);
        this.p_date.textContent = date1;
        //this.p_volume.textContent = data.volume;
        this.p_high.textContent = high;
        this.p_low.textContent = low;
        this.p_open.textContent = open;
        this.p_close.textContent = close;


    }

    eventManagerLinear(row)
    {
        this.p_close.textContent = row[1]; //close
        this.updateVolume(this.id, moment(row[0]).format("YYYY-MM-DD"));
        this.p_date.textContent = moment(row[0]).format("YYYY-MM-DD");
        this.p_high.textContent = row[3];
        this.p_low.textContent = row[4];
        this.p_open.textContent = row[2];
    }

    updateVolume(id, date)
    {
        console.log(date);
        this.socket.emit('update_volume', id, date);
    }
}