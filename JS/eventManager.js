/**
 * Created by leletir & mathieuduverney on 22/12/2016.
 */

var socket = io.connect('https://finance-ats.ddns.net:50022',{secure: true});

var ticker =[]; //nom du ticker selectionné

var old_price = []; //variable contenant le plus vieux prix de l'action
var new_price = []; //variable contenant le prix le plus récent

var id_ticker=[]; //id du ticker

var displayTicker=[]; //variable pour afficher les tickers

var length; //longueur du tableau

var graphManagerObject = new graphManager(socket);
var windowsManagerObject = new windowsManager();
var generatorTicketObjet = new generatorTicket(socket);
var actionObjet = new action();
var retourPython = new strategy(socket);
var pageManager = new changePage();

var action_List_Triee_Ticker = [];// liste des ticker
var id_market_en_memoire;//id du market en memoire, il permet de connaitre le marché en memoire
var lettre_selectionner_en_cours; //interval de lettre que l'on souhaite affiché, on le stock coté client
// car on n'en a pas besoin coté serveur donc inutile de l'envoyer

var action_List_Triee_Ticker = [];// liste des ticker
var id_market_en_memoire;//id du market en memoire, il permet de connaitre le marché en memoire
var lettre_selectionner_en_cours; //interval de lettre que l'on souhaite affiché, on le stock coté client
// car on n'en a pas besoin coté serveur donc inutile de l'envoyer

//var generatorTicketObjet = new generatorTicket();

//Premiere étape : on recoit les nom des marchés et on les affiche

socket.on('name', function (data) {

    // data recu : 2 arrays
    // Le premier contenant les id des Market, le second leur nom

    var generatorModule1Objet = new generatorModule1(data, socket);

});

//récupérer la taille du tableau

socket.on('length', function (data){
    //console.log("longeur recue:"+data);

    length=data;

});

//Récupérer toutes les actions :

socket.on('action', function (data) {

    // on a 4 tableau, on push chaque données recues dans chaque tableau
    // ATTENTION : le server envoie autant d'array qu'il trouve d'action (et pas un seul array contenant toutes les actions) d'où le push

    ticker.push(data.ticker);
    old_price.push(data.old);
    new_price.push(data.new);
    id_ticker.push(data.id_symbol);

    //pour arreter la boucle

    if(ticker.length==length)
    {
        for (var i = 0; i < ticker.length; i++) {

            //on remplis une fois que le tableau ticker est complet
            action_List_Triee_Ticker[i] = ticker[i];
        }

        //on trie les actions pour les mettre dans l'ordre alhabetique
        action_List_Triee_Ticker.sort();


        afficheAction();


        ticker=[]; //je vide tout
        old_price=[];
        new_price=[];
        id_ticker=[];

    }
});




