/**
 * Created by leletir on 19/12/2016.
 */
const EventEmitter = require('events');
class selectActionFromGivenExchangeClass extends EventEmitter{ // On hérite de notre constante
    //Constructor
    constructor(connection,id_exchange_received, socket){
        super(); // appelle constructeur de la classe mère
        this.id_exchange=[];
        this.connection = connection;
        this.id_exchange_received = id_exchange_received;
        this.socket = socket;
        this.on('selectExchangeFinish',this.selectDailyPriceForAGivenAction);
    };
    //Méthodes
    selectActionFromBdd(id_exchange_received)
    {
        //console.log("id recu:"+id_exchange_received);
        this.connection.c.query('SELECT id FROM symbol WHERE id_exchange =?',id_exchange_received).then((function(rows) {
            // on rentre les valeur dans le tableau
            for (var i = 0; i < rows.length; i++) {
                this.id_exchange[i] = rows[i].id; //tableau de toutes les id des actions du marchés sélectionnés
            }
            this.socket.emit('length', this.id_exchange.length);
            this.emit('selectExchangeFinish'); // Quand c'est fini on emet un event qui va déclencher l'execution de la seconde méthode
        }).bind(this));
    }
    convertDate(inputFormat) { // fonction pour afficher la date dans le bon format
        function pad(s) { return (s < 10) ? '0' + s : s; }
        var d = new Date(inputFormat);
        return [pad(d.getDate()), pad(d.getMonth()+1), d.getFullYear()].join('/');
    }
    selectDailyPriceForAGivenAction()
    {
        for(var k=0; k<this.id_exchange.length;++k) // a optimiser //incrémentation post fixé k++ = une opération de plus
        {
            this.connection.c.query('SELECT ticker,MIN(date) AS min, MAX(date) as max, id_symbol FROM symbol INNER JOIN daily_price ON symbol.id = daily_price.id_symbol WHERE symbol.id=? ', this.id_exchange[k]).then((function (rows) {
                // on envoie coté client les données récupérées ci-dessus
                this.socket.emit('action', {
                    ticker: rows[0].ticker,
                    old: this.convertDate(rows[0].min),
                    new: this.convertDate(rows[0].max),
                    id_symbol: rows[0].id_symbol
                });
            }).bind(this));
        }
    }
};
// Exportation
exports.selectActionFromGivenExchangeClass = selectActionFromGivenExchangeClass;



