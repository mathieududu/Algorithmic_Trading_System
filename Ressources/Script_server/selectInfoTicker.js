/**
 * Created by leletir on 21/12/2016.
 */
class selectInfoTicker {
    constructor(connection,socket){
        this.connection = connection;
        this.socket = socket;
    }
    selectInfoTickerFromBdd(id)
    {
        this.connection.c.query('SELECT name, sector FROM symbol WHERE id= ?', id).then((function(rows) {
            this.socket.emit('retour_search_info', { name: rows[0].name, sector: rows[0].sector});
        }).bind(this));
    }
    getVolume(id,date)
    {
        this.connection.c.query('SELECT volume FROM daily_price WHERE id_symbol='+id+' AND Date=\''+date+'\'').then((function(rows) {
            this.socket.emit('retour_volume_info', rows[0]);
        }).bind(this));
    }
}
exports.selectInfoTickerClass = selectInfoTicker;