/**
 * Created by leletir on 16/12/2016.
 */
var PythonShell = require('python-shell');
const fs = require('fs');
const mapNameToScript = JSON.parse(fs.readFileSync('./Ressources/Config/stratsToPython.json'));
class nodeToPythonStrat {
    constructor(data,socket) {

        this.socket = socket;
        this.strat_name = data.strat_name;
        this.script_name = mapNameToScript[this.strat_name];
        this.date_start = data.date_start;
        this.date_end = data.date_end;
        this.ticker_id = data.ticker_id;
        this.option = data.option;
        this.share = data.share;
        this.stop_loss = data.stop_loss;

        this.args = ['ticker_id', this.ticker_id, 'date_start', this.date_start, 'date_end', this.date_end, 'option', this.option, 'share', this.share, 'stop_loss', this.stop_loss];


        this.options = {
            mode: 'text',
            scriptPath: './Ressources/Python/',
            args: this.args
        };

        this.runPython();
    }
    runPython()
    {
        //console.log(this.options);
        PythonShell.run('' + this.script_name + '', this.options, (function (err, results) {
            if (err) throw err;
            // results is an array consisting of messages collected during execution
            this.socket.emit('Python_Response',results);
            //console.log(results);
        }).bind(this));
    }
}

exports.nodeToPythonStratClass = nodeToPythonStrat;


