/**
 * Created by mathieuduverney on 01/02/2017.
 */
class selectMarket {
    //Constructor
    constructor(connection,socket){
        this.connection = connection;
        this.socket = socket;
    }
    selectMarket_()
    {
        this.connection.c.query("SELECT id,name, currency,timezone_offset,country, abbrev FROM exchange").then((function (rows) {

            // on remplit les tableau correctement
            let _market = [];
            for (let i = 0; i < rows.length; i++) {
                _market.push([rows[i].country, [rows[i].currency,rows[i].timezone_offset,rows[i].abbrev, rows[i].name,rows[i].id]]);

            }
            //on envoie coté client l'id et le nom des market a afficher
            this.socket.emit('name', {market: _market});
        }).bind(this));
    }
}
exports.selectMarketClass = selectMarket;