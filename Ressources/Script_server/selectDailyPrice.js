/**
 * Created by leletir on 19/12/2016.
 */
class selectDailyPrice {
    //Constructor
    constructor(connection, id_ticker,socket){
        this.connection = connection;
        this.id_ticker = id_ticker;
        this.socket = socket;
    }
    //Méthodes
    selectDailyPriceFromBdd()
    {
        this.connection.c.query('SELECT ticker, date, Open, High, Close, Low FROM daily_price INNER JOIN symbol ON daily_price.id_symbol = symbol.id WHERE id_symbol= ?', this.id_ticker).then((function(rows) {
            let daily_price_linear = [];
            let daily_price_candle = []
            for (var i = 0; i < rows.length; i++)
            {
                // pour obtenir la date en MS obligatoire pour Highchart
                daily_price_linear.push([(rows[i].date).getTime(), rows[i].Close]);
                daily_price_candle.push([rows[i].Open, rows[i].High, rows[i].Low]);
            }
            daily_price_linear.reverse();
            daily_price_candle.reverse();
            // on envoie les données
            this.socket.emit('ticker_name', rows[0].ticker);
            this.socket.emit('data_chart', { linear: daily_price_linear, candle: daily_price_candle});
        }).bind(this));
    }
}
exports.selectDailyPriceClass = selectDailyPrice;