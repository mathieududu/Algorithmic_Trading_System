# coding=utf-8
from pyalgotrade.tools import yahoofinance
from pyalgotrade.barfeed import yahoofeed
from pyalgotrade.technical import ma
from pyalgotrade import strategy
from pyalgotrade import plotter
from pyalgotrade.stratanalyzer import returns
from pyalgotrade.stratanalyzer import sharpe
from pyalgotrade.utils import stats
import json

import os, sys
import createCSV
import selectTickerNameFromIdSymbol


class StrategySMA(strategy.BacktestingStrategy):
    def __init__(self, feed, instrument, smaPeriod, order, cash,positionTook):
        strategy.BacktestingStrategy.__init__(self, feed, cash)
        self.position = None
        self.instrument = instrument
        self.setUseAdjustedValues(True)
        self.sma = ma.SMA(feed[instrument].getPriceDataSeries(), smaPeriod)
        self.positionTook = positionTook

    def getSMA(self):
        return self.sma

    def onEnterOk(self, position):
        # Buy = 0
        execInfo = position.getEntryOrder().getExecutionInfo()
        #infoFinal = str(str(execInfo.getDateTime()).split(' ')[0]) + ' 0 '  + str(execInfo.getPrice())
        infoFinal = {}
        infoFinal['Date'] = str(execInfo.getDateTime()).split(' ')[0]
        infoFinal['Operation'] = 0
        infoFinal['Price'] = execInfo.getPrice()
        infoFinal['Fee'] = execInfo.getCommission()
        #self.positionTook = json.dumps(infoFinal)
        self.positionTook.append({'order':infoFinal})
        #obj_json = self.info('- BUY ' +  ' - ' + ticker + ' - $%.2f/share' % (execInfo.getPrice()))
        #print(infoFinal)
        #with open('Bookmarks.txt', 'w') as f:
         #   json.dump(obj_json, f)

    def onEnteredCanceled(self, position):
        self.position = None

    def onExitOk(self, position):
        #Sell = 1
        execInfo = position.getExitOrder().getExecutionInfo()
        infoFinal = {}
        infoFinal['Date'] = str(execInfo.getDateTime()).split(' ')[0]
        infoFinal['Operation'] = 1
        infoFinal['Price'] = execInfo.getPrice()
        infoFinal['Fee'] = execInfo.getCommission()
        # self.positionTook = json.dumps(infoFinal)
        self.positionTook.append({'order':infoFinal})
        # obj_json = self.info('- BUY ' +  ' - ' + ticker + ' - $%.2f/share' % (execInfo.getPrice()))
        #print(infoFinal)
        #with open('Bookmarks.txt', 'w') as f:
         #   json.dump(obj_json1, f)
        self.position = None

    def onExitCanceled(self, position):
        self.position.exitMarket()

    def onBars(self, bars):
        if self.sma[-1] == None:
            return

        bar = bars[self.instrument]

        if self.position is None:
            if bar.getPrice() < self.sma[-1]:
                self.position = self.enterLong(self.instrument, order, True)

        elif bar.getPrice() > self.sma[-1] and not self.position.exitActive():
            self.position.exitMarket()

def deleteCSV(tick):

    fileName = tick + ".csv"
    os.remove(fileName)

def main(tick, year, cash, smaPeriod, order,positionTook):

    # Load CSV file
    feed = yahoofeed.Feed()
    fileName = tick + ".csv"
    feed.addBarsFromCSV(tick, fileName)

    # Evaluate Strategy
    strategySMA = StrategySMA(feed, tick, smaPeriod, order, cash,positionTook)

    # Attach a returns analyzers to the strategy.
    retAnalyzer = returns.Returns()
    strategySMA.attachAnalyzer(retAnalyzer)
    sharpeRatioAnalyzer = sharpe.SharpeRatio()
    strategySMA.attachAnalyzer(sharpeRatioAnalyzer)

    # Attach the plotter to the strategy.
    plt = plotter.StrategyPlotter(strategySMA)
    # Include the SMA in the instrument's subplot to get it displayed along with the closing prices.
    plt.getInstrumentSubplot(tick).addDataSeries("SMA", strategySMA.getSMA())

    # Set portfolio cash
    strategySMA.getBroker().setCash(cash)
    initial = strategySMA.getBroker().getEquity()

    # Run the strategy
    strategySMA.run()

    # Print the results

    positionTook.append({'initial': json.dumps(initial)})
    #print('Initial portfolio value: $%.2f' % initial)

    final = strategySMA.getBroker().getEquity()
    positionTook.append({'final': json.dumps(final)})
    #print('Final portfolio value: $%.2f' % final)

    net = final - initial
    if net > 0:
        positionTook.append({'gain': net})
        #print('Net gain: $%.2f' % net)
    else:
        positionTook.append({'gain': net})
        #print('Net loss: $%.2f' % net)

    percentage = (final - initial) / initial * 100
    if percentage > 0:
        positionTook.append({'percentage': json.dumps(percentage)})
        #print('Percentage gain: +%.2f%%' % percentage)
    else:
        positionTook.append({'percentage': json.dumps(percentage)})
        #print('Percentage loss: %.2f%%' % percentage)

    #print('Annual return: %.2f%%' % (retAnalyzer.getCumulativeReturns()[-1] * 100))
    positionTook.append({'Annual_return': json.dumps(retAnalyzer.getCumulativeReturns()[-1] * 100)})
    #print('Average daily return: %.2f%%' % (stats.mean(retAnalyzer.getReturns()) * 100))
    positionTook.append({'daily_return': stats.mean(retAnalyzer.getReturns()) * 100})
    #print('Std. dev. daily return: %.4f' % (stats.stddev(retAnalyzer.getReturns())))
    positionTook.append({'Std_dev': stats.stddev(retAnalyzer.getReturns())})
    #print('Sharpe ratio: %.2f' % (sharpeRatioAnalyzer.getSharpeRatio(0)))
    positionTook.append({'sharpe': sharpeRatioAnalyzer.getSharpeRatio(0)})

    deleteCSV(tick)

    # Plot the strategy
    #plt.plot()

if __name__ == '__main__':

    #id_symbol = '112'
    #date_start = '2015-12-07'
    #date_end = '2016-12-06'
    #option = int('10')
    #share = int('1000')
    #stop_loss = '00.00'

    id_symbol = sys.argv[1:][1]
    date_start = sys.argv[1:][3]
    date_end = sys.argv[1:][5]
    option = int(str(sys.argv[1:][7]))
    share = int(str(sys.argv[1:][9]))
    positionTook = []
    #stop_loss = int(sys.argv[1:][11])

    #print(id_symbol)
    #print(date_start)
    #print(date_end)
    #print(option)
    #print(share)
    #print(stop_loss)

    id_symbol_object = selectTickerNameFromIdSymbol.selectTicker(id_symbol)
    ticker = id_symbol_object.selectFromDb()

    CSV = createCSV.createCSVModule(id_symbol,date_start,date_end,ticker)
    CSV.writingCSV()

    #print(ticker)
    #tick = 'ms.mi'
    year = 2015
    cash = 1000000
    #smaPeriod = 10
    #order = 1000
    smaPeriod = option
    order = share
    main(ticker, year, cash, smaPeriod, order, positionTook)
    print(json.dumps(positionTook))
