# coding=utf-8
import subprocess
import sys
import datetime
import MySQLdb as mdb

class createCSVModule:
    """Classe créant un CSV pour pouvoir utiliser PyAlgoTrade :
        """

    def writingCSV(self):
        # Obtain a database connection to the MySQL instance
        db_host = 'localhost'
        db_user = 's_mUser'
        db_pass = 'atsbdd'
        db_name = 'securities_testing'
        con = mdb.connect(db_host, db_user, db_pass, db_name)

        with con:
            cur = con.cursor()
            #cur.execute('SELECT Date, Open, High, Low, Close, Volume, `Adj Close` FROM securities_testing.daily_price WHERE id_symbol='+self.id_symbol+'AND Date='+'\''+self.date_start+'\'')
            cur.execute(
                'SELECT Date, Open, High, Low, Close, Volume, Adj_Close FROM securities_testing.daily_price WHERE id_symbol=' + self.id_symbol + ' AND Date >= ' + '\'' + self.date_start + '\''+' AND Date <= ' + '\'' + self.date_end + '\'')
            data = cur.fetchall()
            csv_data = 'Date,Open,High,Low,Close,Volume,Adj Close' + '\n'
            for d in data:
                csv_data +=  str(d[0]) + ',' + str(d[1]) + ',' + str(d[2]) + ',' + str(d[3]) + ',' + str(d[4]) + ',' + str(d[5]) + ',' + str(d[6]) + '\n'
            filename = '' + self.ticker + '.csv'
            csv_out = open(filename, 'w')
            csv_out.write(csv_data)
            csv_out.close()

    def __init__(self, id_symbol, date_start, date_end, ticker):  # Notre méthode constructeur
        """Pour l'instant, on ne va définir qu'un seul attribut"""
        self.id_symbol = id_symbol
        self.date_start = date_start
        self.date_end = date_end
        self.ticker = ticker





