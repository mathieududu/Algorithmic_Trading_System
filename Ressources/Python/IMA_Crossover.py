from pyalgotrade import strategy
from pyalgotrade.technical import ma
from pyalgotrade.technical import cross
from pyalgotrade import plotter
from pyalgotrade.barfeed import yahoofeed
from pyalgotrade.stratanalyzer import returns
from pyalgotrade.utils import stats
from pyalgotrade.stratanalyzer import sharpe

import os, sys
import json
import createCSV
import selectTickerNameFromIdSymbol



class SMACrossOver(strategy.BacktestingStrategy):
    def __init__(self, feed, instrument, smaPeriod, positionTook):
        strategy.BacktestingStrategy.__init__(self, feed)
        self.__instrument = instrument
        self.__position = None
        # We'll use adjusted close values instead of regular close values.
        self.setUseAdjustedValues(True)
        self.__prices = feed[instrument].getPriceDataSeries()
        self.__sma = ma.SMA(self.__prices, smaPeriod)
        self.positionTook = positionTook

    def getSMA(self):
        return self.__sma

    def onEnterCanceled(self, position):
        self.__position = None

    def onExitOk(self, position):
        # Sell = 1
        execInfo = position.getExitOrder().getExecutionInfo()
        infoFinal = {}
        infoFinal['Date'] = str(execInfo.getDateTime()).split(' ')[0]
        infoFinal['Operation'] = 1
        infoFinal['Price'] = execInfo.getPrice()
        infoFinal['Fee'] = execInfo.getCommission()
        # self.positionTook = json.dumps(infoFinal)
        self.positionTook.append({'order': infoFinal})
        # obj_json = self.info('- BUY ' +  ' - ' + ticker + ' - $%.2f/share' % (execInfo.getPrice()))
        # print(infoFinal)
        # with open('Bookmarks.txt', 'w') as f:
        #   json.dump(obj_json1, f)
        self.__position = None

    def onExitCanceled(self, position):
        # If the exit was canceled, re-submit it.
        self.__position.exitMarket()

    def onEnterOk(self, position):
        # Buy = 0
        execInfo = position.getEntryOrder().getExecutionInfo()
        #infoFinal = str(str(execInfo.getDateTime()).split(' ')[0]) + ' 0 '  + str(execInfo.getPrice())
        infoFinal = {}
        infoFinal['Date'] = str(execInfo.getDateTime()).split(' ')[0]
        infoFinal['Operation'] = 0
        infoFinal['Price'] = execInfo.getPrice()
        infoFinal['Fee'] = execInfo.getCommission()
        #self.positionTook = json.dumps(infoFinal)
        self.positionTook.append({'order':infoFinal})

    def onBars(self, bars):
        # If a position was not opened, check if we should enter a long position.
        if self.__position is None:
            if cross.cross_above(self.__prices, self.__sma) > 0:
                shares = int(self.getBroker().getCash() * 0.9 / bars[self.__instrument].getPrice())
                # Enter a buy market order. The order is good till canceled.
                self.__position = self.enterLong(self.__instrument, shares, True)
        # Check if we have to exit the position.
        elif not self.__position.exitActive() and cross.cross_below(self.__prices, self.__sma) > 0:
            self.__position.exitMarket()

def deleteCSV(tick):

    fileName = tick + ".csv"
    os.remove(fileName)

def main(tick,positionTook,cash, smaPeriod):
    # Load the yahoo feed from the CSV file
    feed = yahoofeed.Feed()
    fileName = tick + ".csv"
    feed.addBarsFromCSV(tick, fileName)
    #feed.addBarsFromCSV("orcl", "orcl-2000.csv")
    # Evaluate the strategy with the feed's bars.
    myStrategy = SMACrossOver(feed,tick, smaPeriod, positionTook)
    myStrategy.getBroker().setCash(cash)
    initial = myStrategy.getBroker().getEquity()
    # Attach a returns analyzers to the strategy.
    returnsAnalyzer = returns.Returns()
    myStrategy.attachAnalyzer(returnsAnalyzer)

    # Attach a returns analyzers to the strategy.
    retAnalyzer = returns.Returns()
    myStrategy.attachAnalyzer(retAnalyzer)
    sharpeRatioAnalyzer = sharpe.SharpeRatio()
    myStrategy.attachAnalyzer(sharpeRatioAnalyzer)


    # Set portfolio cash
    myStrategy.getBroker().setCash(cash)
    initial = myStrategy.getBroker().getEquity()

    # Run the strategy.
    myStrategy.run()

    positionTook.append({'initial': json.dumps(initial)})
    # print('Initial portfolio value: $%.2f' % initial)

    final = myStrategy.getBroker().getEquity()
    positionTook.append({'final': json.dumps(final)})
    # print('Final portfolio value: $%.2f' % final)

    net = final - initial
    if net > 0:
        positionTook.append({'gain': net})
        # print('Net gain: $%.2f' % net)
    else:
        positionTook.append({'gain': net})
        # print('Net loss: $%.2f' % net)

    percentage = (final - initial) / initial * 100
    if percentage > 0:
        positionTook.append({'percentage': json.dumps(percentage)})
        # print('Percentage gain: +%.2f%%' % percentage)
    else:
        positionTook.append({'percentage': json.dumps(percentage)})
        # print('Percentage loss: %.2f%%' % percentage)

    # print('Annual return: %.2f%%' % (retAnalyzer.getCumulativeReturns()[-1] * 100))
    positionTook.append({'Annual_return': json.dumps(retAnalyzer.getCumulativeReturns()[-1] * 100)})
    # print('Average daily return: %.2f%%' % (stats.mean(retAnalyzer.getReturns()) * 100))
    positionTook.append({'daily_return': stats.mean(retAnalyzer.getReturns()) * 100})
    # print('Std. dev. daily return: %.4f' % (stats.stddev(retAnalyzer.getReturns())))
    positionTook.append({'Std_dev': stats.stddev(retAnalyzer.getReturns())})
    # print('Sharpe ratio: %.2f' % (sharpeRatioAnalyzer.getSharpeRatio(0)))
    positionTook.append({'sharpe': sharpeRatioAnalyzer.getSharpeRatio(0)})

    deleteCSV(tick)

if __name__ == '__main__':

    id_symbol = sys.argv[1:][1]
    #id_symbol = str(5)
    date_start = sys.argv[1:][3]
    #date_start = str('2015-08-01')
    #date_end = str('2016-08-01')
    date_end = sys.argv[1:][5]
    option = int(str(sys.argv[1:][7]))
    share = int(str(sys.argv[1:][9]))
    positionTook = []

    id_symbol_object = selectTickerNameFromIdSymbol.selectTicker(id_symbol)
    ticker = id_symbol_object.selectFromDb()

    CSV = createCSV.createCSVModule(id_symbol, date_start, date_end, ticker)
    CSV.writingCSV()
    cash = 1000000;
    main(ticker,positionTook,cash,option)
    print(json.dumps(positionTook))
