var express = require('express');
var app = express();
const https = require('https');
var mongoose = require('mongoose');
var passport = require('passport');
var flash    = require('connect-flash');
var morgan       = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser   = require('body-parser');
var session      = require('express-session');
var fs           = require('fs');
var configDB     = require('./config/database.js');
//var fs = require('fs');

var options = {
    key: fs.readFileSync('./Ressources/Certificats/key.pem'),
    cert: fs.readFileSync('./Ressources/Certificats/cert.pem')
};

var serverPort = 50022;

// configuration ===============================================================
mongoose.connect(configDB.url); // connect to our database

require('./config/passport')(passport); // pass passport for configuration

// set up our express application
app.use(morgan('dev')); // log every request to the console
app.use(cookieParser()); // read cookies (needed for auth)
app.use(bodyParser()); // get information from html forms

app.set('view engine', 'ejs'); // set up ejs for templating

// required for passport
app.use(session({ secret: 'ilovescotchscotchyscotchscotch' })); // session secret
app.use(passport.initialize());
app.use(passport.session()); // persistent login sessions
app.use(flash()); // use connect-flash for flash messages stored in session

// routes ======================================================================
require('./app/routes.js')(app, passport); // load our routes and pass in our app and fully configured passport

// Workers can share any TCP connection
// In this case it is an HTTPS server

var server = https.createServer(options, app);
var io = require('socket.io')(server);
var mysql = require('promise-mysql');

var connectionFonction = require('./Ressources/Script_server/connexionBDD').connection;
var connection = connectionFonction();

var sectOnInit = require('./Ressources/Script_server/selectMarket.js').selectMarketClass;
var selectActionFromGivenExchange = require('./Ressources/Script_server/selectActionFromGivenExchange.js').selectActionFromGivenExchangeClass;
var selectDailyPrice = require('./Ressources/Script_server/selectDailyPrice.js').selectDailyPriceClass;
var selectInfoTicker = require('./Ressources/Script_server/selectInfoTicker.js').selectInfoTickerClass;
var nodeToPythonStrat = require('./Ressources/Script_server/nodeToPythonStrat.js').nodeToPythonStratClass;
//var nodeToPythonLive = require('./Ressources/Script_server/nodeToPythonLive.js').nodeToPythonLiveClass;
// déclarer les dossier static obligatoire pour le CSS, JS et autres

app.use(express.static(__dirname + '/CSS'));
app.use(express.static(__dirname + '/JS'));
app.use(express.static(__dirname + '/Ressources'));

app.use(function (req, res, next) {

    // Website you wish to allow to connect
    res.setHeader('Access-Control-Allow-Origin', '*');

    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type, Authorization');

    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    res.setHeader('Access-Control-Allow-Credentials', true);

    // Pass to next layer of middleware
    next();
});

var router = express.Router();

app.use('/', router);



//Gestion des sockets

io.on('connection', function (socket) {

    // à la connection on load l'id et le name des marker

    var selectMarketOnInit = new sectOnInit(connection, socket);
    selectMarketOnInit.selectMarket_();

    // 1er listener : recoit l'id du market qu'on a selectionné partie client

    socket.on('sendIdMarket', function (data) {

        var selectMarket = new selectActionFromGivenExchange(connection, data, socket);
        selectMarket.selectActionFromBdd(data);
    });

    // listener 2 : on recoit l'id du ticker qu'on a selectionné partie client

    socket.on('sendIdTicker', function (data) {

        var daily_price = new selectDailyPrice(connection, data, socket);
        daily_price.selectDailyPriceFromBdd();
    });

    socket.on('search_info', function (id) { //Récupère le nom et le secteur pour un ticker donné quand on click sur un ticker du Module 2
        //console.log('arrive au serveur:' +id);
        let searchInfo = new selectInfoTicker(connection, socket)
        searchInfo.selectInfoTickerFromBdd(id);
    });

    socket.on('update_volume', function (id, date) {
        //console.log('arrive au serveur:' +id);
        let searchInfo = new selectInfoTicker(connection, socket)
        searchInfo.getVolume(id, date);
    });

    socket.on('requireStratJSON', function () { // On renvoi le fichier JSON
        socket.emit('sendStratJSON', JSON.parse(fs.readFileSync('./Ressources/Config/strat.json')));
    });

    socket.on('run_strat', function (data) { // On renvoi le fichier JSON
        var nodeToPythonStratObjet = new nodeToPythonStrat(data, socket);
        console.log("run");
        console.log(data);
    });
});

// port sur lequel on écoute

server.listen(serverPort, function () {
    console.log('server up and running at %s port', serverPort);
});
